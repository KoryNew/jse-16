package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskToProject(final String projectId, final String taskId) throws AbstractException;

    Task unbindTaskFromProject(final String projectId, final String taskId) throws AbstractException;

    List<Task> findAllTasksByProjectId(final String id) throws EmptyIdException;

    Project removeProjectById(final String id) throws EmptyIdException;

    Project removeProjectByIndex(final int index) throws IncorrectIndexException;

    Project removeProjectByName(final String name) throws EmptyNameException;

}
