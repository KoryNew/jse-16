package ru.tsk.vkorenygin.tm.api.entity;

import java.util.Date;

public interface IHasCreated {

    Date getCreateDate();

    void setCreateDate(Date createDate);

}
