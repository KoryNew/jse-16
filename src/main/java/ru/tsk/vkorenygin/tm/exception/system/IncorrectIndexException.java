package ru.tsk.vkorenygin.tm.exception.system;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(final Throwable cause) {
        super(cause);
    }

    public IncorrectIndexException(final String value) {
        super("Error! This value '" + value + "' is not a number.");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect.");
    }

}
